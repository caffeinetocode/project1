// Cocos2dxCustomMacros.h
// Contains extra Macro definitions for Project1

// Namespace definitions
#ifdef __cplusplus
    #define NS_C2C_BEGIN namespace caffeieneToCode {
    #define NS_C2C_END   }
    #define USING_NS_C2C using namespace caffeieneToCode
    #define USING_NS_CC_ST using namespace cocostudio
#else
    #define NS_C2C_BEGIN 
    #define NS_C2C_END 
    #define USING_NS_C2C 
    #define USING_NS_CC_ST
#endif 