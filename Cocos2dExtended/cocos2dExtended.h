// Cocos2dxCustom.h
// Contains includes and macros for our that extend cocos2dx

// Macro definitions should be defined first.
#include "cocos2dExtendedMacros.h"

// Include cocos header files here.
#include "cocos2d.h"
#include "cocostudio\cocoStudio.h"