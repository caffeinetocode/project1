#ifndef PROJECT1_SHAREDSOURCE_EXAMPLESCENE_H_
#define PROJECT1_SHAREDSOURCE_EXAMPLESCENE_H_

#include "cocos2dExtended.h"

class ExampleScene : public cocos2d::Scene
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // a callback function
    void menuCloseCallback( cocos2d::Ref* pSender );

    // implement the "static create()" method manually
    CREATE_FUNC( ExampleScene );

CC_CONSTRUCTOR_ACCESS:
    ExampleScene();
    virtual ~ExampleScene();
};

class ExampleSceneFromFile : public cocos2d::Scene
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // a callback function
    void menuCloseCallback( cocos2d::Ref* pSender );

    // implement the "static create()" method manually
    CREATE_FUNC( ExampleSceneFromFile );

CC_CONSTRUCTOR_ACCESS:
    ExampleSceneFromFile();
    virtual ~ExampleSceneFromFile();
};

#endif // PROJECT1_SHAREDSOURCE_EXAMPLESCENE_H_