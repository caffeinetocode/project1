#ifndef PROJECT1_SHAREDSOURCE_NODEUTILS_H_
#define PROJECT1_SHAREDSOURCE_NODEUTILS_H_

#include "cocos2dExtendedMacros.h"
#include "cocos2d.h"
#include "cocostudio\cocoStudio.h"

NS_C2C_BEGIN

class CC_DLL NodeUtils : public cocos2d::Ref
{
public:
    virtual bool init();

    // Returns a shared instance of Configuration
    static NodeUtils* getInstance();

    // Purge the shared instance of Configuration
    static void destroyInstance();

protected:
    virtual ~NodeUtils();

private:
    NodeUtils();

    static NodeUtils* ms_sharedNodeUtils;
};

NS_C2C_END

#endif // PROJECT1_SHAREDSOURCE_NODEUTILS_H_