#ifndef PROJECT1_SHAREDSOURCE_NODECLASSTEMPLATE_H_
#define PROJECT1_SHAREDSOURCE_NODECLASSTEMPLATE_H_

#include "cocos2dExtended.h"

class NodeClassTemplate :
    public cocos2d::Node
{
public:

    CREATE_FUNC( NodeClassTemplate );
    virtual bool init();

CC_CONSTRUCTOR_ACCESS:
    NodeClassTemplate();
    virtual ~NodeClassTemplate();
};

#endif // PROJECT1_SHAREDSOURCE_NODECLASSTEMPLATE_H_