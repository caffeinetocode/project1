///////////////////
// ExampleScene.cpp
//
// Description:
//     This contains example code on how to manipulate a scene
//   when creating a scene entirely from code vs from loading
//   from a scene file.
//     Both exmples will load from a file to have the same structure
//   and so that we do not have to re-create the file in raw code
//   and keep them in sync
//     The pseudo-non-file scene will show how to use resources
//   as if they were not started yet.
//     The load from file example actually starts a lot of the
//   resources at load time, the example will show that we do not
//   need to start/use the resources again because they are already
//   being used.


#include "stdafx.h"
#include "ExampleScene.h"

USING_NS_CC;
USING_NS_CC_ST;

////////////////
// Example Scene

ExampleScene::ExampleScene()
{
}

ExampleScene::~ExampleScene()
{
}

bool ExampleScene::init()
{
    /////////////////
    // Initialization

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // Set Scene to origin from OpenGLView. All child nodes will be positioned
    // relative to their parent.
    this->setPosition( Vec2( origin.x, origin.y ) );

    // Get root node from scene file, this contains all sub-nodes.
    // This example will show how to use scene resources as if they
    // were not auto-started.
    Node *pNodeFromFile = SceneReader::getInstance()->
        createNodeWithSceneFile( "publish/Level00001.json" );

    // Add node to this Scene
    this->addChild( pNodeFromFile, 1, 1 );

    // Get a ComRender component by tag.
    ComRender *pHeroComRender = (ComRender*) ( pNodeFromFile->getChildByTag( 10001 )->getComponent( "CCArmature" ) );

    // Get Armature child node attached to component.
    Armature *pHeroArmature = dynamic_cast<Armature *>( pHeroComRender->getNode() );


    ////////////////
    // Node Movement

    // pNodeFromFile defaults to position ( Vec2::ZERO ).

    // Set Hero's postition.
    pNodeFromFile->getChildByTag( 10001 )->setPosition( Vec2( 1000, 100 ) );

    // DO NOT USE
    // Do not position the child Armature node.
    // Instead position the parent node
    pHeroArmature->setPosition( Vec2( 1000, 100 ) );


    ////////////
    // Animation

    // Play an Animation.
    pHeroArmature->getAnimation()->play( "Left" );

    // DO NOT USE
    // You can also play by index (the order in which animations were created)
    // Don't use indeces though. If you delete an animation, the indeces will change.
    pHeroArmature->getAnimation()->playWithIndex( 1 );


    ////////
    // Audio

    // Get an audio component by name.
    ComAudio *pAudio = (ComAudio*) ( pNodeFromFile->getComponent( "CCBackgroundAudio" ) );

    // Play audio.
    pAudio->playBackgroundMusic( pAudio->getFile(), pAudio->isLoop() );


    /////////////
    // GUI - Menu

    // Add a "close" icon to exit the progress. It's an autorelease object.
    auto closeItem = MenuItemImage::create(
        "CloseSelected.png",
        "CloseNormal.png",
        CC_CALLBACK_1( ExampleScene::menuCloseCallback, this )
        );

    closeItem->setPosition( Vec2( origin.x + visibleSize.width - closeItem->getContentSize().width / 2,
        origin.y + visibleSize.height - closeItem->getContentSize().height / 2 ) );

    // Create menu. Stacks MenuItems on top of eachother
    // by increasing the zorder of each one.
    auto menu = Menu::create( closeItem, NULL );
    menu->setPosition( Vec2( origin.x, origin.y ) );
    this->addChild( menu, 1 );


    /////////////////
    // GUI - LabelTTF

    // Create and initialize a label.
    auto label = LabelTTF::create( "", "Arial", 24 );

    // Position the label on the center of the screen.
    label->setPosition( Vec2( origin.x + visibleSize.width / 2,
        origin.y + visibleSize.height - label->getContentSize().height ) );

    // Add the label as a child to this scene.
    this->addChild( label, 1 );


    /////////
    // Sprite

    // Add "ExampleScene" splash screen.
    auto sprite = Sprite::create( "1345324713901.jpg" );

    // Position the sprite on the center of the screen.
    sprite->setPosition( Vec2( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y ) );

    // Add the sprite as a child to this scene.
    this->addChild( sprite, 0 );

    return true;
}
    
void ExampleScene::menuCloseCallback( cocos2d::Ref* pSender )
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}


/////////////////////////
// Example Scene From File

ExampleSceneFromFile::ExampleSceneFromFile()
{
}

ExampleSceneFromFile::~ExampleSceneFromFile()
{
}

bool ExampleSceneFromFile::init()
{
    /////////////////
    // Initialization

    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // Set Scene to origin from OpenGLView. All child nodes will be positioned
    // relative to their parent.
    this->setPosition( Vec2( origin.x, origin.y ) );

    // Get root node from scene file, this contains all sub-nodes.
    // This example will show how to use scene resources as if they
    // were auto-started.
    Node *pNodeFromFile = SceneReader::getInstance()->
        createNodeWithSceneFile( "publish/Level00001.json" );

    // Add node to this Scene
    this->addChild( pNodeFromFile, 1, 1 );


    ////////////////
    // Node Movement

    // pNodeFromFile defaults to position ( Vec2::ZERO ).
    // All child nodes are positioned relative to each parent node.


    ////////////
    // Animation

    // Default animations are loaded from the file.
    // Do not override them.


    ////////
    // Audio

    // Audio from the pNodeFromFile is played automatically.

    /////////////
    // GUI - Menu

    // Menus are loaded from the file and put in their default state.


    /////////////////
    // GUI - LabelTTF

    // Labels are loaded from the file.


    /////////
    // Sprite

    // Sprites are loaded from the file.

    // Replace scene.
    // Director::getInstance()->replaceScene(firstScene);

    return true;
}
    
void ExampleSceneFromFile::menuCloseCallback( cocos2d::Ref* pSender )
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}