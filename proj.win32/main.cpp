#include "main.h"
#include "AppDelegate.h"
#include "cocos2dExtended.h"

USING_NS_CC;

int APIENTRY _tWinMain( HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPTSTR    lpCmdLine,
    int       nCmdShow )
{
    UNREFERENCED_PARAMETER( hPrevInstance );
    UNREFERENCED_PARAMETER( lpCmdLine );

    // create the application instance
    AppDelegate app;
    return Application::getInstance()->run();
}

int wmain( int argc, wchar_t* argv[] )
{
    UNREFERENCED_PARAMETER( argc );
    UNREFERENCED_PARAMETER( argv );

    // create the application instance
    AppDelegate app;
    return Application::getInstance()->run();
}