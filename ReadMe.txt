Welcome to Project1 :)

Author: Michael J. Metz.

// ReadMe Syntax

    <Variable> - Replace with the appropriate text
        ex. replace Feature<number> with Feature1

        
// Repository Layout
    
    +-- cocos2d  <-- The Cocos2d source code.
    |
    +-- Cocos2dExtended  <- Our custom extensions to the cocos2d source code.
    |   |
    |   f-- cocos2dExtended.h  <-- Master include file!
    |   |                          * Include in any source file you create that needs a cocos2d
    |   |                            class, a cocosStudio class or a cocos2dExtended
    |   |                            (caffeineToCode) class or macro.
    |   |                          * Avoid including in headers where possible (instead include
    |   |                            in .cpp file).
    |   |                          * Do not include if adding classes to Cocos2dExtended.
    |   |
    |   f-- cocos2dExtendedMacros.h  <-- Contains cocos2dExtended (caffeineToCode) macros that
    |                                     can be used in multiple project code files. 
    +-- CocosStudio
    |   +-- assets  <-- Contains all assets. Proper folders and files will be copied to the
    |   |   |            Resources folder at build time.
    |   |   |
    |   |   +-- <AssetType>
    |   |   |   +-- <AssetID>
    |   |   |       +-- <publish>  <-- Scene Artists use these if not using an Animation or GUI
    |   |   |                          Animators and GUI Artists use these assets too.
    |   |   |
    |   |   +-- <publish>  <-- Scene Artists use these Animation and GUI assets.
    |   |
    |   +-- ccsprojs  <-- Open these files within SceneManager after opening Project1.xml.ccsproj.
    |                     to edit Cocos Studio projects.
    |
    +-- proj.<platform>  <-- Contains source code, build files and other files needed to build
    |                        our app for that platform.
    |
    +-- Resources  <-- Contain assets for Developers to use, updates from assets folder when
    |   |              developer runs Developer Update Script or builds application.
    |   |              
    |   |              !!! Should not use these directly in code as these resources to use
    |   |                  are specified in the scene files and should not be hard coded.
    |   +-- <AssetType>
    |   |   +-- <AssetID>
    |   |       +-- <publish>  <-- Developers can use these if not using an Animation or
    |   |                          GUI or Scene.
    |   |
    |   +-- <publish>  <-- Developers use these Animation, GUI, and Scene assets.
    |   |
    |   f-- <AppName>.plist  <-- Config file copied at build time from SharedSource.
    |                            Use this in code.
    |
    +-- SharedBuild  <-- Contains files for building that are used by 2 or more platforms.
    |                    Ex. The AssetUpdate.cmd script is used for Win32 and Windows Phone builds.
    |
    +-- SharedSource  <-- Contains the main source code for our app. Used by all
    |                     platforms.
    |
    +-- Templates  <-- If creating a new class (Ex. NodeController, SingletonUtility, Scene, you
    |                  can find templates here, as well as examples of Cocos2d code being used.
    |
    f-- <AppName>.xml.ccsproj <-- Open this in SceneEditor to manage all CocosStudio projects.
                                 This is for all GUI Artists, Animators, and Scene Editors.     

        
// Asset User Artists and Developer Workflow

    !!! ALL COMMIT MESSAGES WILL HAVE THE FORMAT:
        <BranchID> - <CommitMessage>
        
        If you merge a branch, use the source <BranchID> and a summary of the change
        Ex. If I merge Feature3 into Development I would
        use the message "Feature3 - Added AssetUserUpdate script and BuildScript script."

    This project has the following branch types (Used for <BranchIDs>):
        * Master
            * Hotfixes are branched from here and merged into Master and Development.
            * Contains the tagged version of the application.
        * Development
            * All Feature and Issue work is branched off from this and re-merged later.
        * Release<VersionNumber>
            * Is branched from Development, it is tested then merged to Master.
            * If bugs are found, they are fixed in this branch and then merged to Master
              and Development.
            * Branch name ex. Release1.0.0000.0
        * Feature<number>
            * Branch off from Development, then merge later.
            * Use when adding a new code feature.
            * Branch name ex. Feature1, Feature2, ... 
        * Issue<number>
            * Branch off from Development, then merge later.
            * Use when fixing an issue in the code.
        * Hotfix<number>
            * Branch off from Master, then merge to Master and Development at the same
              time.
            * Use when fixing an issue in the code that originated in Master.
        * ArtFeature<number>
            * Branch off from Development, then merge later.
            * Use when adding a new art feature.
            * Branch name ex. Feature1, Feature2, ... 
        * ArtIssue<number>
            * Branch off from Development, then merge later.
            * Use when fixing an art issue.
        * ArtHotfix<number>
            * Branch off from Master, then merge to Master and Development at the same
              time.
            * Use when fixing an art issue that originated in Master.
    
    When referring to "art" in the branches, this is for artist resources stored in
    Git.
        * For example:
            * Animation .json files
            * Scene .json files
            * Custom property .json files
            * Tile map *.tmx files
        * See more info in the Artist Workflow section
        
    !!! To update code or Asset User files:
    1. Run the AssetUpdate.cmd (Developers only have to do this if they need a new or
       update asset, Asset User Artists must do this every time).
    2. Get the latest origin/Development branch.
    3. Create an appropriate branch, (ex. Feature3, ArtIssue12) from development
       based on what you are working on.
    4. Make you changes and commit your branch when necessary (with appropriate messages).
    5. Publish your branch and create a pull request if change is complicated (or you lack
       permission).
    6. Merge your branch into an appropriate branch (use directions in branch descriptions
       above). Add an appropriate merge commit message.
        <SourceBranchID> - <SummaryOfChange>
    7. Remove your published branch and delete it in your local repository.

        
// Artists (Asset Creator Artists and Asset User Artists) Workflow
    
    Art Asset management:
        * All assets will be stored with the following folder structure
            * assets\<AssetType>\<AssetID>
            !!! <AssetType> can be Animations, Custom_Property, GUI, Music, or Scenes.
            * <AssetID> is a unique name for your asset.
                !!! SEARCH ALL <AssetType> FOLDERS TO MAKE SURE YOUR <AssetID> IS NOT IN USE.
            !!! Sound effects will be within each respective <AssetID>'s publish folder.
                and not in the Music folder.
    
    assets\<AssetType>\<AssetID> folder structure:
        +-- <AssetID>  <-- Contains the raw data files to create the published asset (if needed)
            |              Ex. Vector images, .wav/.alac/.flac, .png sprite animation frames,
            |              anything that can't be used directly by Asset User Artists or
            |              Developers.
            +-- Archive
            |   +-- <YYYY\MM\DD>  <-- Copy and save any resources from your <AssetID> folder.
            |       |                 Keep folder structure the same (ex. keep publish folder
            |       |                 resources in a sub-folder called publish.)
            |       f-- ReadMe.txt  <-- Optional file that contains comments on why the resource
            |                           was archived.
            |
            +-- publish  <-- Contains assets ready to be used by Asset User Artists or Developers
            |                This could be .png format raster images, .mp3/.midi format sound
            |                effects or a spritesheet from an animation.
            |                
            |                MODIFY PUBSLISHED ASSETS DIRECTLY If the published asset does not
            |                come from parent art files
            |                   Ex. An image that was created in raster form only
            |                       (but we use vector art, so this case won't happen)
            |                   Ex2. A tile map TMX file will be modified in here.
            |
            +-- <OtherFolders>  <-- Create other folders to organize the resources needed to
                                    create your assets.
                             
    There are two types of artists, Asset Creator Artists and Asset User Artists. The following
    will outline their tasks.

        Asset Creator Artists
            * Create assets to be used by others.
            !!! WORK OUT OF GOOGLE DRIVE.
            * Should not have to check into Git.
            * All assets will be created in the <AssetID> or <AssetID>\publish folder.
            * Some examples
                * Create a Character sheet vector image for an Animation in the <AssetID> folder
                (someone could also create sound effects in here)
                    * Then you would export each body part (for the different facing directions)
                      as a published resource *.png and output sound effects in the proper
                      format (ex. .mp3).
                    * These *.pngs would then be used by Animators.
                * Create Music
                    * Store raw .wav or lossless .flac/.alac and store separate tracks (ex.
                      guitar, piano, vocals... ).
                    * Publish the music in the proper format in a publish folder.
        
        Asset User Artists
            * These are artists who use assets.
            !!!1 Always run the AssetUpdate.cmd script before you modify any of your projects
                 Then get the latest Development branch from Git.
            !!!2 You will update your work in your local Git repository and then check in.
                * Follow the Asset User Artist workflow from earlier.
            !!!3 Animators and GUI editors, after you export your animation or GUI, you must
                 manually copy and paste your published animation in
                 assets\publish\<AssetIDFilesFromExportedAnimation>
                 back to Google Drive's assets\publish\ folder.
            * Scene artists will not have to export as this is taken care of in the Build
              Script.
            !!! Scene Artists MUST use exported GUIs and Animations in assets\publish.
                * Non-GUI and Non-Animation assets can be used from assets\<AssetType>\<AssetID>.
            !!! GUI and Animators must use assets\<AssetType>\<AssetID>\publish assets.


// General code conventions
    !!! Comments
        * Comment a brief description of WHAT a function/class does at the top of a function/class.
            * DO this for as many classes/functions as possible, unless it is really obvious what
              it does and the parameter input is simple.
        !!! DO NOT COMMENT elsewhere without reason. It just adds needless text that we have
            to keep in sync with the code.
            * Instead code with Really Obvious Code (R.O.C.). All variables and classes should
              be named such that you do not need comments to describe what it is doing.
            !!! DO NOT comment HOW code works. 
            * The only reason you will be allowed to comment within code is to describe
              WHY that code works.
              
              Ex.
                  // Running an action Retains the Hero Node and Action
                  // so it won't get deleted next frame.
                  pHero->runAction( pAction );
                  
              Note that most Cocos Developers should know this, so you do not need to put
              "Why" comments on everything that retains objects (consider your audience).
            
            * Write shorter why comments if possible
              
              Ex.
                  // Retains hero and action
                  pHero->runAction( pAction );
        * Optionally read more here:
            * http://blog.codinghorror.com/code-tells-you-how-comments-tell-you-why/


// Batch/Cmd Script conventions
    * All functions return Outputs with quotes removed.
    * All inputs are not allowed to have " in them unless paired consecutively like this:
      "inputp""aram"
    * Any function inputs with special batch characters must be quoted ex. :function
      "%InputVarWithSpecialChars%"