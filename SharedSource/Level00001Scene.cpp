#include "stdafx.h"
#include "HeroController.h"
#include "Level00001Scene.h"


USING_NS_CC;
USING_NS_CC_ST;
USING_NS_C2C;

Level00001Scene::Level00001Scene()
{
}

Level00001Scene::~Level00001Scene()
{
}

// on "init" you need to initialize your instance
bool Level00001Scene::init()
{
    /////////////////
    // Initialization

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // Set Scene to origin from OpenGLView. All child nodes will be positioned
    // relative to their parent.
    this->setPosition( Vec2( origin.x, origin.y ) );

    // Get root node from scene file, this contains all sub-nodes.
    Node *pNodeFromFile = SceneReader::getInstance()->
        createNodeWithSceneFile( "publish/Level00001.json" );

    // Add node to this Scene.
    this->addChild( pNodeFromFile, 1, 1 );

    // Get Hero Node.
    Node* pHero = pNodeFromFile->getChildByTag( 10001 );

    // Attach controlling logic.
    HeroController* pHeroController = HeroController::create();
    pHero->addChild( pHeroController, 1, "Controller" );

    // Get a ComRender component by tag.
    ComRender *pHeroComRender = dynamic_cast<ComRender*>( pHero->getComponent( "CCArmature" ) );

    // Get Armature child node attached to component.
    Armature *pHeroArmature = dynamic_cast<Armature*>( pHeroComRender->getNode() );

    // Set Hero's postition.
    pHero->setPosition( Vec2( 1000, 100 ) );


    /////////////
    // GUI - Menu

    // Add a "close" icon to exit.
    auto closeMenuImage = MenuItemImage::create(
        "CloseSelected.png",
        "CloseNormal.png",
        CC_CALLBACK_1( Level00001Scene::menuCloseCallback, this )
        );
    closeMenuImage->setPosition( Vec2( visibleSize.width - closeMenuImage->getContentSize().width / 2,
        visibleSize.height - closeMenuImage->getContentSize().height / 2 ) );

    // MenuItems are retained.
    auto menu = Menu::create( closeMenuImage, NULL );

    menu->setPosition( Vec2::ZERO );

    // Children are retained by scene
    this->addChild( menu, 1 );


    ////////////////////
    // Add other Sprites

    auto sprite = Sprite::create( "1345324713901.jpg" );

    sprite->setPosition( Vec2( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y ) );
    this->addChild( sprite, 0 );

    pHeroArmature->setScale( 3.0f );


    //////////
    // Actions

    // Running an action retains the target.
    runDefaultActions( pHeroArmature );

    return true;
}


void Level00001Scene::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void Level00001Scene::runDefaultActions( Armature* pTargetArmature )
{
    // Sequence is autorelease but retains actions.
    float duration = 10.0f;
    Sequence* seq2 = Sequence::create(
        CallFuncN::create( []( Node* pTarget )
            {
                if ( Armature* pTargetArmature = dynamic_cast<Armature*>( pTarget ) )
                    pTargetArmature->getAnimation()->play( "Left" );
                else
                    CCLOGWARN( "Node* pTarget is not an Armature*" );
            } ), 
        CCSpawn::create( MoveBy::create( duration, Point( -800, 70 ) ), EaseOut::create( CCScaleTo::create( duration, 0.3333f ), 5.0f ), NULL),
        CallFuncN::create(
            []( Node* pTarget )
            {
                if ( Armature* pTargetArmature = dynamic_cast<Armature*>( pTarget ) )
                    pTargetArmature->getAnimation()->play( "Right" );
                else
                     CCLOGWARN( "Node* pTarget is not an Armature*" );
            } ),
        CCSpawn::create(
            MoveBy::create( duration, Point( 800, -70 ) ),
            EaseIn::create( CCScaleTo::create( duration, 3.0f ), 5.0f ),
            NULL),
        NULL );

    // Retains target and action.
    pTargetArmature->runAction( RepeatForever::create( seq2 ) );
}