#ifndef PROJECT1_SHAREDSOURCE_INITIALIZERSCENE_H_
#define PROJECT1_SHAREDSOURCE_INITIALIZERSCENE_H_

#include "cocos2dExtended.h"

class Level00001Scene : public cocos2d::Scene
{
public:

    // Implement the "static create()" method.
    CREATE_FUNC( Level00001Scene );

    // Init the scene.
    virtual bool init();

    void runDefaultActions( cocostudio::Armature* pTargetArmature );

    /////////////////////
    // Callback functions

    void menuCloseCallback( cocos2d::Ref* pSender );

CC_CONSTRUCTOR_ACCESS:
    Level00001Scene();
    virtual ~Level00001Scene();
};

#endif // PROJECT1_SHAREDSOURCE_INITIALIZERSCENE_H_
