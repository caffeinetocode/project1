#ifndef PROJECT1_SHAREDSOURCE_HERO_H_
#define PROJECT1_SHAREDSOURCE_HERO_H_

#include "cocos2dExtended.h"

NS_C2C_BEGIN

class HeroController : public cocos2d::Node
{
public:

    CREATE_FUNC( HeroController );
    virtual bool init();

CC_CONSTRUCTOR_ACCESS:
    HeroController();
    virtual ~HeroController();
};

NS_C2C_END

#endif // PROJECT1_SHAREDSOURCE_HERO_H_