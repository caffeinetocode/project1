#include "stdafx.h"
#include "NodeUtils.h"

USING_NS_CC;
NS_C2C_BEGIN

NodeUtils* NodeUtils::ms_sharedNodeUtils = nullptr;

NodeUtils::NodeUtils()
{
}

NodeUtils::~NodeUtils()
{
    ms_sharedNodeUtils = nullptr;
}

NodeUtils* NodeUtils::getInstance()
{
    if (!ms_sharedNodeUtils)
    {
        ms_sharedNodeUtils = new NodeUtils();
        ms_sharedNodeUtils->init();
    }
    
    return ms_sharedNodeUtils;
}

void NodeUtils::destroyInstance()
{
    CC_SAFE_RELEASE_NULL(ms_sharedNodeUtils);
}

bool NodeUtils::init()
{
    return true;
}

NS_C2C_END