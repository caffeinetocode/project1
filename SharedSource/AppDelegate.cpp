#include "stdafx.h"
#include "AppDelegate.h"
#include "Level00001Scene.h"
#include <vector>

USING_NS_CC;

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // Load configuration before creating the Director.
    // Show FPS, and other functions are received from configuration file.
    // Director deletes this when end() is called.
    Configuration* conf = CCConfiguration::getInstance();
    conf->loadConfigFile( "Project1.plist" );

    bool isFullScreen = conf->getValue( "project1.full_screen", Value( false ) ).asBool();

    // Initialize director.
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if ( !glview )
    {
        if ( isFullScreen )
        {
            glview = GLView::createWithFullScreen( "Project1" );
        }
        else
        {
            std::vector<Value> defaultValue;
            defaultValue.push_back( Value( 1280 ) );
            defaultValue.push_back( Value( 720 ) );
            int x = conf->getValue( "project1.resolution", Value( ValueVector( defaultValue ) ) ).asValueVector().at( 0 ).asInt();
            int y = conf->getValue( "project1.resolution", Value( ValueVector( defaultValue ) ) ).asValueVector().at( 1 ).asInt();
            glview = GLView::createWithRect( "Project1", Rect( 0, 0, x, y ) );
        }
        CCLOG( "\n\nConfig Settings:", NULL );

        // GLView is retained by director.
        director->setOpenGLView( glview );
    }

    // Create a scene. It's an autorelease object.
    auto scene = Level00001Scene::create();

    // Runs the scene and also retains it.
    director->runWithScene( scene );

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too.
void AppDelegate::applicationDidEnterBackground()
{
    Director::getInstance()->stopAnimation();

    // If you use SimpleAudioEngine, it must be paused.
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// This function will be called when the app is active again.
void AppDelegate::applicationWillEnterForeground()
{
    Director::getInstance()->startAnimation();

    // If you use SimpleAudioEngine, it must resume here.
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
